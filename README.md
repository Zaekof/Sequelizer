Launcher Sequelize

// Initialisation
// --> $ sequelize init

// Création d'un model
// --> $ sequelize model:create --name User --attributes first_name:string,last_name:string,bio:text

// Docs
// --> $ https://sequelize.readthedocs.io/en/v3/docs/getting-started/